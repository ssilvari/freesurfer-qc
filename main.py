#!/bin/env python
import os
import pandas as pd
from os.path import join, dirname, splitext, isdir, basename, isfile

from tqdm import tqdm
from nilearn import plotting

def get_subject_data(subjects_dir):
    """Looks for subjects in data"""
    folder_list = [join(subjects_dir, f) for f in os.listdir(subjects_dir)]
    folder_list = [f for f in folder_list if isdir(f)]

    subjects = [basename(f) for f in folder_list]

    brainmask = [join(f, 'mri/brainmask.mgz') for f in folder_list]
    brainmask = [b if isfile(b) else pd.NA for b in brainmask]

    aseg = [join(f, 'mri/aseg.mgz') for f in folder_list]
    aseg = [a if isfile(a) else pd.NA for a in aseg]

    data = {
        'root': folder_list,
        'brainmask': brainmask,
        'aseg': aseg
    }
    return pd.DataFrame(data=data, index=subjects).dropna(how='any')


if __name__ == "__main__":
    subjects_dir = '/data/asclepios/share/private/cabinade/ADNI/MRI_FS/'
    out_dir = '/data/asclepios/share/private/ssilvari/MRI_CHECK/'

    df = get_subject_data(subjects_dir)

    print(f'Subjects loadded from: {subjects_dir}')
    print(df.head())
    print('Number of subjects (images) found:', len(df.shape))

    for subject, row in tqdm(df.iterrows()):
        g = plotting.plot_roi(roi_img=row['aseg'], bg_img=row['brainmask'], display_mode='z', alpha=0.6,
                                title=f'Subject: {subject}', cmap='Paired', threshold=0, black_bg=False,
                                output_file=join(out_dir, f'{subject}.png'))

    