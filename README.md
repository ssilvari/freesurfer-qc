# FreeSurfer Quality Check

## Instructions

Clone the repo using:

```bash
git clone https://gitlab.inria.fr/ssilvari/freesurfer-qc.git
cd freesurfer-qc
```

Change `line 31-32` defining the subjetcs dirrectory `subjects_dir` and where the results will be saved `out_dir`.

```python
subjects_dir = 'YOUR_SUBJECTS_DIRECTORY'
out_dir = 'YOUR_RESULTS_DIRECTORY'
```

Run it (or submit a job):

```bash
./main.py
```
